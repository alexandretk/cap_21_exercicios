public class Conta {

		//Atributos
	protected double saldo;
	private int numeroConta; 
	private Titular titular; 

	public void saca(double valor) { 
		this.saldo -= valor; 
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}
	
	public void printSaldo() { 
		System.out.println(this.saldo);
	}

	public void setNome(String nome) {
		titular.setNome(nome);
	}
	public String getNome() {
		return titular.getNome();
	}
	public void setCpf(String cpf) {
		titular.setCpf(cpf);
	}
	public String getCpf() {
		return titular.getCpf();
	}

		//Construtor
	public Conta(double saldo, int numeroConta, String nome, String cpf) {
		this.titular= new Titular(nome, cpf);
		this.saldo=saldo;
		this.numeroConta= numeroConta;
	}

	
} 

