public class AplicacoesFundo  extends Conta {
		

		private double taxaRendimento;

		
			public AplicacoesFundo(double saldo, int numeroConta,  double taxaRendimento, String nome, String cpf ) {
			super(saldo, numeroConta, nome, cpf);
			this.taxaRendimento= taxaRendimento; 
			}

	public double getTaxaRendimento() {
		return taxaRendimento;
	}

	public void setTaxaRendimento(double taxaRendimento) {
		this.taxaRendimento = taxaRendimento;
	}

	public void atualizaSaldo( ) { 
		this.saldo += this.saldo * taxaRendimento; 
	} 
	
}
