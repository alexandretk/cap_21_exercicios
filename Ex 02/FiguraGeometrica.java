package Ex02;

public class FiguraGeometrica {

	// Atributos
		private int numLados;
		private double perimetro;
		private double area;
		
	// Metodos
		public int getNumLados() {
			return numLados;
		}
		
		public void setNumLados(int numLados) {
			this.numLados = numLados;
		}
		
		public double getPerimetro() {
			return perimetro;
		}
		
		public void setPerimetro(double perimetro) {
			this.perimetro = perimetro;
		}
		
		public double getArea() {
			return area;
		}
		
		public void setArea(double area) {
			this.area = area;
		}
			//Construtor
		public FiguraGeometrica(int numLados, double perimetro, double area) {
			
			this.numLados = numLados;
			this.perimetro = perimetro;
			this.area = area; 
		}
	
		
}
