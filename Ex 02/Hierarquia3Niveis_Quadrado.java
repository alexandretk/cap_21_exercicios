package Ex02;

public class Hierarquia3Niveis_Quadrado extends Hierarquia3Niveis_FiguraQuatroLados {

	private double tamanhoLado;
	private double diagonal;

	public double getTamanhoLado() {
		return tamanhoLado;
	}

	public void setTamanhoLado(double tamanhoLado) {
		this.tamanhoLado = tamanhoLado;
	}
	
	public Hierarquia3Niveis_Quadrado( double tamanhoLado) {
		super(tamanhoLado, tamanhoLado, java.lang.Math.sqrt( 2* tamanhoLado*tamanhoLado), java.lang.Math.sqrt( 2* tamanhoLado*tamanhoLado));
		diagonal= java.lang.Math.sqrt( 2* tamanhoLado*tamanhoLado);
	}
	

	
}

		
