package Ex02;

public class Hierarquia2Niveis_Retangulo extends FiguraGeometrica {

	//Atributos
	private double ladoMaior;
	private double ladoMenor;
	

	//Construtor
	public Hierarquia2Niveis_Retangulo(double ladoMaior, double ladoMenor)  {
		super(4,2*( ladoMaior + ladoMenor), ladoMaior*ladoMenor);

	}


public void setLadoMaior(double ladoMaior) {
	this.ladoMaior = ladoMaior;
}

public double getLadoMaior() {
	return ladoMaior;
}

public void setLadoMenor(double ladoMenor) {
	this.ladoMaior = ladoMenor;
}

public double getLadoMenor() {
	return ladoMenor;
}
}