package Ex02;

public class Hierarquia3Niveis_Triangulo extends FiguraGeometrica {
	// Atributos
	private double lado1;
	private double lado2;
	private double lado3;
	private double base;
	private double altura;
	//construtor
	public Hierarquia3Niveis_Triangulo(double base, double altura, 
			double lado1, double lado2, double lado3) {
		super(3, lado1+ lado2+ lado3, base*altura*0.5);
	
	}
    //setters e getters
	
	public void setlado1(double lado1) {
		this.lado1 = lado1;
	}

	public double getlado1() {
		return lado1;
	}
	public void setlado2(double lado2) {
		this.lado2 = lado2;
	}

	public double getlado2() {
		return lado2;
	}
	public void setlado3(double lado3) {
		this.lado3 = lado3;
	}

	public double getlado3() {
		return lado3;
	}
	

	public void setbase(double base) {
		this.base = base;
	}

	public double getbase() {
		return base;
	}
	
	public void setaltura(double altura) {
		this.altura = altura;
	}

	public double getaltura() {
		return altura;
	}

	
}
