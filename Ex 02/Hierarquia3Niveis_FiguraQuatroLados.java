package Ex02;

public class Hierarquia3Niveis_FiguraQuatroLados extends FiguraGeometrica{
		//Atributos
			private double tamanhoLadoMaior;
			private double tamanhoLadoMenor;
			private double diagonalMaior;
			private double diagonalMenor;
			private double maiorAngulo;
			
			//Metodos
			public double getMaiorAngulo() {
				return maiorAngulo;
			}
			
			public void setMaiorAngulo(double maiorAngulo) {
				this.maiorAngulo = maiorAngulo;
			}

			public double getTamanhoLadoMaior() {
				return tamanhoLadoMaior;
			}

			public void setTamanhoLadoMaior(double tamanhoLadoMaior) {
				this.tamanhoLadoMaior = tamanhoLadoMaior;
			}
			
			public double getTamanhoLadoMenor() {
				return tamanhoLadoMenor;
			}


			public void setTamanhoLadoMenor(double tamanhoLadoMenor) {
				this.tamanhoLadoMenor = tamanhoLadoMenor;
			}

			public double getDiagonalMaior() {
				return diagonalMaior;
			}


			public void setDiagonalMaior(double diagonalMaior) {
				this.diagonalMaior = diagonalMaior;
			}
			
			public double getDiagonalMenor() {
				return diagonalMenor;
			}


			public void setDiagonalMenor(double diagonalMenor) {
				this.diagonalMenor = diagonalMenor;
			}


			public Hierarquia3Niveis_FiguraQuatroLados( double tamanhoLadoMaior, double tamanhoLadoMenor, double diagonalMenor, double diagonalMaior) {
				super(4, 2*( tamanhoLadoMaior + tamanhoLadoMenor), diagonalMenor*diagonalMaior);
			} 
		
			
}
