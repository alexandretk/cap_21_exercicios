package Ex02;

public class Hierarquia2Niveis_TrianguloRetangulo extends FiguraGeometrica {


	//Construtor
	public Hierarquia2Niveis_TrianguloRetangulo(double base, double altura, 
			double lado1, double lado2, double lado3) {
		super(3, lado1+ lado2+ lado3, base*altura*0.5);
	
	}
	
	//Atributos
	private double lado1;
	private double lado2;
	private double lado3;
	private double angulo1;
	private double angulo2;
	private double angulo3;
	private double base;
	private double altura;
	

	//Setters e Getters
	public void setlado1(double lado1) {
		this.lado1 = lado1;
	}

	public double getlado1() {
		return lado1;
	}
	public void setlado2(double lado2) {
		this.lado2 = lado2;
	}

	public double getlado2() {
		return lado2;
	}
	public void setlado3(double lado3) {
		this.lado3 = lado3;
	}

	public double getlado3() {
		return lado3;
	}
	
	public void setangulo1(double angulo1) {
		this.angulo1 = angulo1;
	}

	public double getangulo1() {
		return angulo1;
	}
	public void setangulo2(double angulo2) {
		this.angulo2 = angulo2;
	}

	public double getangulo2() {
		return angulo2;
	}
	public void setangulo3(double angulo3) {
		this.angulo3 = angulo3;
	}

	public double getangulo3() {
		return angulo3;
	}

	public void setbase(double base) {
		this.base = base;
	}

	public double getbase() {
		return base;
	}
	
	public void setaltura(double altura) {
		this.altura = altura;
	}

	public double getaltura() {
		return altura;
	}
}
