package Ex02;

public class Hierarquia3Niveis_TrianguloObtusangulo extends Hierarquia3Niveis_Triangulo {

	private double angulo1;
	private double angulo2;
	private double angulo3;

	//Construtor
	public Hierarquia3Niveis_TrianguloObtusangulo(double base, double altura, 
			double lado1, double lado2, double lado3) {
			super(base, altura, lado1, lado2, lado3);
	
	}
		
	public void setangulo1(double angulo1) {
		this.angulo1 = angulo1;
	}

	public double getangulo1() {
		return angulo1;
	}
	public void setangulo2(double angulo2) {
		this.angulo2 = angulo2;
	}

	public double getangulo2() {
		return angulo2;
	}
	public void setangulo3(double angulo3) {
		this.angulo3 = angulo3;
	}

	public double getangulo3() {
		return angulo3;
	}

}
