package Ex02;

public class Hierarquia2Niveis_Quadrado extends FiguraGeometrica {

	private double tamanhoLado;


	
	public Hierarquia2Niveis_Quadrado( double tamanhoLado) {
		super(4, 4*tamanhoLado, tamanhoLado*tamanhoLado );
	}
	
	public double getTamanhoLado() {
		return tamanhoLado;
	}

	public void setTamanhoLado(double tamanhoLado) {
		this.tamanhoLado = tamanhoLado;
	}
	
}
