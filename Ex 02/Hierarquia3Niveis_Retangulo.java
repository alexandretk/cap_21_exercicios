package Ex02;

public class Hierarquia3Niveis_Retangulo extends Hierarquia3Niveis_FiguraQuatroLados {
 

	private double diagonal;


	//Construtor
	public Hierarquia3Niveis_Retangulo(double ladoMaior, double ladoMenor)  {
	super(ladoMaior, ladoMenor, java.lang.Math.sqrt( ladoMenor*ladoMenor + ladoMaior*ladoMaior), java.lang.Math.sqrt( ladoMenor*ladoMenor + ladoMaior*ladoMaior) );
	diagonal= java.lang.Math.sqrt( ladoMenor*ladoMenor + ladoMaior*ladoMaior);
	}

	public double getDiagonal() {
		return diagonal;
	}

	public void setDiagonal(double diagonal) {
		this.diagonal = diagonal;
	}
	
}
